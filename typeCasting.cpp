//============================================================================
// Name        : casting.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Event.h"
#include "OpenFileEvent.h"
#include "CopyFileEvent.h"

void displayEvent(Event *event) {
	std::cout << "Received event: " << event->getEventType() << " ";
	if (event->getEventType() == "OpenFileEvent") {
		OpenFileEvent *openEvent = static_cast<OpenFileEvent*>(event); // type casting happens during compilation
		std::cout << "Trying to open file: " << openEvent->getFileName() << "\n";
	}
}
void doSomething(int *A, int size_A) {
	for (int i = 0; i < size_A; ++i) {
		for (int j = 0; j < size_A; ++j) {
		}
		std::cout << A[i] << "\n";
	}
}

void displayEventDynamicCast(Event *event) {
	std::cout << "Received event: " << event->getEventType() << " ";
	OpenFileEvent *openEvent = dynamic_cast<OpenFileEvent*>(event);
	// type casting happens while program is performing
	// is dynamic_cast fails, pointer will be set at 0, which should be checked
	if (openEvent) { // checks if dynamic_cast is successful
		std::cout << "Successfully casted to OpenFileEvent* "
				<< openEvent->getFileName() << "\n";
	}
}

int main() {
//	float aNumber(3.5);
////	int MyFloat::operator() // example of cast operator for types defined by programmer
//	int someOtherNumber = static_cast<int>(aNumber); // static_cast - casting at compilation
//	int someOtherNumberB = (int) aNumber; // c style casting
//	int someOtherNumberC = int (aNumber); // function casting

	Event gev;
	OpenFileEvent oev("a.txt");
	CopyFileEvent cev("a.txt", "b.txt");

	const Event *ev = &gev;
	displayEvent(const_cast<Event*>(ev)); // const_cast removes the const or volatile property from an object
	ev = &oev;
	displayEvent(const_cast<Event*>(ev));

	return 0;
}
