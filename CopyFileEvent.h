/*
 * CopyFileEvent.h
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef COPYFILEEVENT_H_
#define COPYFILEEVENT_H_

#include "Event.h"

class CopyFileEvent: public Event {
public:
	CopyFileEvent(std::string sourceFile, std::string destinationFile);
	virtual ~CopyFileEvent();

	const std::string& getSourceFile() const {return sourceFile;}
	const std::string& getDestinationFile() const {return destinationFile;}

private:
	static const std::string eventType;
	std::string sourceFile;
	std::string destinationFile;
};

#endif /* COPYFILEEVENT_H_ */
